<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGObrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('g_obras', function (Blueprint $table) {
            $table->id('pk_id_obra');
            $table->text('obra')->nullable()->comment('OBRA O NOMBRE DEL PROYECTO');
            $table->text('descripcion')->nullable()->comment('DESCRIPCION DE LA OBRA');
            $table->integer('fk_user')->nullable()->comment('USUARIO QUIEN REGISTRO O ACTUALIZO REGISTRO');
            $table->integer('activo')->nullable()->default('1');
            $table->integer('fkp_departamento')->nullable()->comment('PARAMETRICA DEL DEPARTAMENTO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('g_obras');
    }
}
