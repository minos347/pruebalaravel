<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGLiberarGarantiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('g_liberar_garantias', function (Blueprint $table) {
            $table->id('pk_id_liberar_garantia');
            $table->text('justificacion')->nullable()->comment('JUSTIFICACION DE LIBERACION DE GARANTIA');
            $table->integer('fkp_tipo_justificacion')->nullable()->comment('PARAMETRICA TIPO DE JUSTIFICACION');
            $table->date('fecha_liberacion')->nullable()->comment('FECHA DE LIBERACION DE LA GARANTIA');
            $table->string('nur_nuri')->nullable()->comment('NUR O NURI PARA EL SEGUIMIENTO DEL DOCUMENTO');
            $table->string('archivo')->nullable()->comment('SUBIR ARCHIVO ADJUNTAR CARTA DE LIBERACION FORMATO PDF');
            $table->integer('fk_user')->nullable()->comment('USUARIO QUIEN REGISTRO O ACTUALIZO REGISTRO');
            $table->integer('activo')->nullable()->default(1);
            $table->timestamps();

            $table->integer('fk_id_garantia')->unsigned()->comment('RELACIONA CON LA TABLA G_GARANTIAS');
            $table->foreign('fk_id_garantia')->references('pk_id_garantia')->on('g_garantias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('g_liberar_garantias');
    }
}
