<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGGarantiaUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('g_garantia_usuarios', function (Blueprint $table) {
            $table->id('pk_id_garantia_usuario');
            $table->string('codigo_garantia')->nullable()->comment('CODIGO DE GARANTIA');
            $table->string('cuce')->nullable()->comment('CUCE DEL PROYECTO');
            $table->string('empresa')->nullable()->comment('EMPRESA DEL PROYECTO');
            $table->string('afianzado')->nullable()->comment('AFIANZADO DE LA EMPRESA');
            $table->integer('fk_id_user')->nullable()->comment('USUARIO ASIGNADO PROYECTO GARANTIA');
            $table->integer('fk_user')->nullable()->comment('USUARIO QUIEN REGISTRO O ACTUALIZO REGISTRO');
            $table->integer('activo')->nullable()->default(1);
            $table->timestamps();
            $table->integer('fk_id_garantia')->unsigned()->comment('RELACIONA CON LA TABLA G_GARANTIAS');
            $table->foreign('fk_id_garantia')->references('pk_id_garantia')->on('g_garantias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('g_garantia_usuarios');
    }
}
