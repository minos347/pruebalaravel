<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGEnviarCorreosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('g_enviar_correos', function (Blueprint $table) {
            $table->id('pk_id_enviar_correo');
            $table->string('correo_usuario')->nullable()->comment('CORREO ELECTRONICO DEL USUARIO');
            $table->string('nombre_usuario')->nullable()->comment('NOMBRE DE USUARIO PARA ENVIAR CORREO');
            $table->integer('fk_user')->nullable()->comment('USUARIO QUIEN REGISTRO O ACTUALIZO REGISTRO');
            $table->integer('activo')->nullable()->default(1);
            $table->timestamps();
            $table->integer('fk_id_garantia')->unsigned()->comment('RELACIONA CON LA TABLA G_GARANTIAS');
            $table->foreign('fk_id_garantia')->references('pk_id_garantia')->on('g_garantias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('g_enviar_correos');
    }
}
