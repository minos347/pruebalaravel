<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('g_empresas', function (Blueprint $table) {
            $table->id('pk_id_empresa');
            $table->string('empresa')->nullable()->comment('NOMBRE DE LA EMPRESA');
            $table->text('representante_legal')->nullable()->comment('REPRESENTANTE LEGAL');
            $table->string('telefono')->nullable()->comment('TELEFONO DE LA EMPRESA');
            $table->string('fax')->nullable()->comment('FAX DE LA EMPRESA');
            $table->string('celular')->nullable()->comment('CELULAR DE LA EMPRESA');
            $table->text('direccion')->nullable()->comment('DIRECCION DE LA EMPRESA');
            $table->string('correo')->nullable()->comment('CORREO DE LA EMPRESA');
            $table->integer('fk_user')->nullable()->comment('USUARIO QUIEN REGISTRO O ACTUALIZO REGISTRO');
            $table->integer('activo')->nullable()->default('1');
            $table->integer('fkp_departamento')->nullable()->comment('PARAMETRICA DEL DEPARTAMENTO');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('g_empresas');
    }
}
