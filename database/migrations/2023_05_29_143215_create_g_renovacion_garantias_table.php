<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGRenovacionGarantiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('g_renovacion_garantias', function (Blueprint $table) {
            $table->id('pk_id_renovacion_garantia');
            $table->string('codigo_garantia')->nullable()->comment('CODIGO DE LA GARANTIA QUE SE ESTA RENOVANDO');
            $table->integer('fkp_tipo_garantia')->nullable()->comment('TIPO DE GARATIA QUE SE ESTA RENOVANDO');
            $table->decimal('importe',15,2)->nullable()->comment('MONTO IMPORTE QUE SE ESTA RENOVANDO');
            $table->integer('fkp_tipo_moneda')->nullable()->comment('TIPO DE MONEDA QUE SE ESTA RENOVANDO');
            $table->date('fecha_vencimiento')->nullable()->comment('FECHA DE VENCIMIENTO QUE SE ESTA RENOVANDO');
            $table->integer('fk_user')->nullable()->comment('USUARIO QUIEN REGISTRO O ACTUALIZO REGISTRO');
            $table->integer('activo')->nullable()->default(1);
            $table->timestamps();

            $table->integer('fk_id_renovacion')->unsigned()->comment('RELACIONA CON LA TABLA G_RENOVACIONS');
            $table->foreign('fk_id_renovacion')->references('pk_id_renovacion')->on('g_renovacions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('g_renovacion_garantias');
    }
}
