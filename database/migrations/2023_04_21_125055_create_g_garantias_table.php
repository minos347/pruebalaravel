<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGGarantiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('g_garantias', function (Blueprint $table) {
            $table->id('pk_id_garantia');
            $table->string('codigo_garantia')->nullable()->comment('CODIGO DE GARANTIA');
            $table->integer('fkp_entidad_financiera')->nullable()->comment('PARAMETRICA ENTIDAD FINANCIERA - BANCO');
            $table->integer('fkp_estado')->nullable()->comment('PARAMETRICA ESTADO DE LA GARANTIA EJCUTADA, LIBERADA, RENOVADA, VIGENTE ETC.');
            $table->decimal('importe', 20, 2)->nullable()->comment('MONTO DOLARES O BOLIVIANOS');
            $table->integer('fkp_moneda')->nullable()->comment('PARAMETRICA TIPO DE MONEDA');
            $table->string('correlativo_tesoreria')->nullable()->comment('NUMERACION DE CARPETAS O ARCHIVOS DE TESORERIA');
            $table->integer('fkp_tipo_garantia')->nullable()->comment('PARAMETRICA TIPO DE GARANTIA');

            $table->integer('fk_id_unidad_organizacional')->nullable()->comment('SERVICIO ID UNIDAD ORGANIZACIONAL');
            $table->string('unidad_organizacional')->nullable()->comment('SERVICIO NOMBRE UNIDAD ORGANIZACIONAL');

            $table->integer('fk_id_proyecto')->nullable()->comment('BD OTRA TABLA EXTERNA Pp_DB_ficha_proyecto ID DE PROYECTO');
            $table->text('nombre_proyecto')->nullable()->comment('BD OTRA TABLA EXTERNA Pp_DB_ficha_proyecto NOMBRE DE PROYECTO');
            $table->string('cuce')->nullable()->comment('CODIGO UNICO DE CONTRATACIONES ESTATALES');

            $table->integer('fk_id_afianzado')->nullable()->default('0')->comment('FORENKEY RELACION CON LA TABLA EMPRESA AFIANZADO CON LA EMPRESA');
            $table->string('afianzado')->nullable()->comment('AFIANZADO CON LA EMPRESA CONTRATISTA');

            $table->string('tramo')->nullable()->comment('TRAMO DEL PROYECTO');
            $table->string('sisin')->nullable()->comment('SISIN DEL PROYECTO');
            $table->date('fecha_vencimiento')->nullable()->comment('FECHA DE VENCIMIENTO');
            $table->date('fecha_emision')->nullable()->comment('FECHA DE EMISION');
            $table->date('fecha_ingreso_abc')->nullable()->comment('FECHA DE INGRESO A LA ABC');
            $table->date('fecha_liberacion')->nullable()->comment('FECHA DE LIBERACION');
            $table->string('verificacion_legal')->nullable()->comment('VERIFICACION LEGAL');
            $table->string('verificacion_tecnica')->nullable()->comment('VERIFICACION TECNICA');
            $table->string('nur_nuri')->nullable()->comment('NUR O NURI DEL DOCUMENTO');
            $table->integer('fk_user')->nullable()->comment('USUARIO QUIEN REGISTRO O ACTUALIZO REGISTRO');
            $table->integer('activo')->nullable()->default('1');
            $table->string('directorio')->nullable()->comment('DIRECTORIO');
            $table->string('numero_contrato')->nullable()->comment('NUMERO DE CONTRATO');
            $table->decimal('importe_ejecutado')->nullable()->comment('IMPORTE EJECUTADO');
            $table->integer('fk_contrato_siin')->nullable()->comment('CONTRATO '); //REVISAR PROCEDENCIA DE ESTE ID
            $table->integer('fk_proyecto_ficha')->nullable()->comment('FOREING KEY DEL PROYECTO FICHA');
            $table->string('codigo_proceso')->nullable()->comment('CODIGO DE PROCESO');
            //            $table->integer('fk_proceso_contratacion')->nullable()->comment('FORENING');
            $table->integer('garantia_requerimiento')->nullable()->default('0')->comment('GARANTIA DE REQUERIMIENTO');
            $table->string('archivo')->nullable()->comment('RUTA DEL ARCHIVO SUBIDO AL SISTEMA');
            $table->timestamps();

            $table->integer('fk_id_empresa')->nullable()->unsigned()->comment('RELACIONA CON LA TABLA G_EMPRESAS');
            $table->foreign('fk_id_empresa')->references('pk_id_empresa')->on('g_empresas');

            $table->integer('fk_id_obra')->nullable()->unsigned()->comment('RELACIONA CON LA TABLA G_OBRAS');
            $table->foreign('fk_id_obra')->references('pk_id_obra')->on('g_obras');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('g_garantias');
    }
}