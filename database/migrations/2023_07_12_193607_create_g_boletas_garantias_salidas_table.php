<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGBoletasGarantiasSalidasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('g_boletas_garantias_salidas', function (Blueprint $table) {
            $table->id('pk_id_boleta_garantia_salida');
            $table->date('fecha_ejecucion')->nullable()->comment('FECHA DE EJECUCION');
            $table->string('cuenta_bancaria')->nullable()->comment('NUMERO DE CUENTA BANCARIA');
            $table->string('numero_deposito')->nullable()->comment('NUMERO DE DEPOSITO');
            $table->date('fecha_boleta')->nullable()->comment('FECHA DE LA BOLETA');
            $table->decimal('monto_boleta',10,2)->nullable()->comment('MONTO DE LA BOLETA');
            $table->integer('fkp_tipo_moneda')->nullable()->comment('TIPO DE MONEDA');
            $table->string('archivo')->nullable()->comment('SUBIR ARCHIVO ADJUNTAR BOLETAS DE PAGO EN FORMATO PDF');
            $table->integer('fk_user')->nullable()->comment('USUARIO QUIEN REGISTRO O ACTUALIZO REGISTRO');
            $table->integer('activo')->nullable()->default(1);
            $table->timestamps();

            $table->integer('fk_id_boleta_garantia')->unsigned()->comment('RELACIONA CON LA TABLA G_BOLETAS_GARANTIAS');
            $table->foreign('fk_id_boleta_garantia')->references('pk_id_boleta_garantia')->on('g_boletas_garantias');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('g_boletas_garantias_salidas');
    }
}
