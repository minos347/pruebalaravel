<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGProyectoCarpetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('g_proyecto_carpetas', function (Blueprint $table) {
            $table->id('pk_id_proyecto_carpeta');
            $table->integer('correlativo')->nullable()->comment('NUMERO DE CORRELATIVO');
            $table->integer('ano')->nullable()->comment('AÑO DE GESTION DEL PROYECTO');
            $table->string('licitacion')->nullable()->comment('NUMERO DE LICITACION');
            $table->text('nombre_proyecto')->nullable()->comment('NOMBRE DEL PROYECTO');
            $table->string('cuce')->nullable()->comment('CUCE NUMERO DE O CODIGO DEL PROYECTO');
            $table->string('numero_contrato')->nullable()->comment('NUMERO DE CONTRATO');
            $table->integer('fk_user')->nullable()->comment('USUARIO QUIEN REGISTRO O ACTUALIZO REGISTRO');
            $table->integer('activo')->nullable()->default(1);
            $table->timestamps();

            $table->integer('fk_id_empresa')->nullable()->unsigned()->comment('RELACIONA CON LA TABLA G_EMPRESAS');
            $table->foreign('fk_id_empresa')->references('pk_id_empresa')->on('g_empresas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('g_proyecto_carpetas');
    }
}
