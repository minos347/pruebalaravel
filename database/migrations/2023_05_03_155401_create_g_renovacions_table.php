<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGRenovacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('g_renovacions', function (Blueprint $table) {
            $table->id('pk_id_renovacion');
            $table->string('codigo_garantia')->nullable()->comment('CODIGO DE LA GARANTIA');
            $table->integer('fkp_tipo_garantia')->nullable()->comment('TIPO DE GARANTIA');
            $table->decimal('importe',20,2)->nullable()->comment('IMPORTE MONTO DE LA GARANTIA');
            $table->integer('fkp_tipo_moneda')->nullable()->comment('TIPO DE MONEDA');
            $table->date('fecha_emision')->nullable()->comment('FECHA DE EMISION');
            $table->date('fecha_ingreso')->nullable()->comment('FECHA DE INGRESO');
            $table->date('fecha_vencimiento')->nullable()->comment('FECHA DE VENCIMIENTO');
            $table->text('justificacion')->nullable()->comment('JUSTIFICACION DE LA RENOVACION');
            $table->string('nur_nuri')->nullable()->comment('NUR O NURI PARA RENOVACION DE LA GARANTIA');
            $table->string('codigo_garantia_renovado')->nullable()->comment('CODIGO DE GARANTIA RENOVADOS');
            $table->integer('modelo_carta')->nullable()->default(0)->comment('MODELO DE CARTA NUMERO');
            $table->string('archivo')->nullable()->comment('ARCHIVO SUBIDO AL SISTEMA');
            $table->integer('fk_user')->nullable()->comment('USUARIO QUIEN REGISTRO O ACTUALIZO REGISTRO');
            $table->integer('activo')->nullable()->default(1);
            $table->timestamps();

            $table->integer('fk_id_garantia')->unsigned()->comment('RELACIONA CON LA TABLA G_GARANTIAS');
            $table->foreign('fk_id_garantia')->references('pk_id_garantia')->on('g_garantias');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('g_renovacions');
    }
}
