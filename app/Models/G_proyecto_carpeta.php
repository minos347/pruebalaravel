<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class G_proyecto_carpeta extends Model
{
    use HasFactory;
    protected $table = 'g_proyecto_carpetas';

    protected $fillable = [
        'pk_id_proyecto_carpeta',
        'correlativo',
        'ano',
        'licitacion',
        'nombre_proyecto',
        'cuce',
        'numero_contrato',
        'fk_user',
        'fk_id_empresa',

    ];
    protected $guarded = ['pk_id_proyecto_carpoeta','created_at','updated_at'];
    protected $primaryKey = 'pk_id_proyecto_carpeta';

    public function G_empresa()
    {
        return $this->belongsTo('App\Models\G_empresa', 'fk_id_empresa', 'pk_id_empresa');
    }
}
