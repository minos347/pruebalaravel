<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class G_liberar_garantia extends Model
{
    use HasFactory;
    protected $table = 'g_liberar_garantias';

    protected $fillable = [
        'pk_id_liberar_garantia',
        'justificacion',
        'fkp_tipo_justificacion',
        'fecha_liberacion',
        'nur_nuri',
        'archivo',
        'fk_user',
        'activo',
        'fk_id_garantia',
    ];
    protected $guarded = ['pk_id_liberar_garantia','created_at','updated_at'];
    protected $primaryKey = 'pk_id_liberar_garantia';

    public function G_garantia()
    {
        return $this->belongsTo('App\Models\G_garantia', 'fk_id_garantia', 'pk_id_garantia');
    }
}
