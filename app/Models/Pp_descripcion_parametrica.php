<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pp_descripcion_parametrica extends Model
{
    use HasFactory;

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pk_id_descripcion_parametrica';

    /**
     * @var array
     */
    protected $fillable = ['fk_id_parametrica', 'codigo', 'descripcion', 'activo', 'created_at', 'updated_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Pp_Parametrica()
    {
        return $this->belongsTo('App\Pp_Parametrica', 'fk_id_parametrica', 'pk_id_parametrica');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ppUnidadesOrganizacionales()
    {
        return $this->hasMany('App\PpUnidadesOrganizacionale', 'fkp_departamento', 'pk_id_descripcion_parametrica');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function Pp_estructura_financiamiento()
    {
        return $this->hasMany('App\Pp_estructura_financiamiento', 'fkp_tipo_convenio', 'pk_id_descripcion_parametrica');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ppAdministracionPoas()
    {
        return $this->hasMany('App\PpAdministracionPoa', 'fkp_gestion', 'pk_id_descripcion_parametrica');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ppTareasFuncionamientos()
    {
        return $this->hasMany('App\PpTareasFuncionamiento', 'fkp_trimestre', 'pk_id_descripcion_parametrica');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ppFormulacionInversiones()
    {
        return $this->hasMany('App\PpFormulacionInversione', 'fkp_superficie_rodadura', 'pk_id_descripcion_parametrica');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ppFormulacion130s()
    {
        return $this->hasMany('App\PpFormulacion130', 'fkp_tipo', 'pk_id_descripcion_parametrica');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ppProgramacionInversiones()
    {
        return $this->hasMany('App\PpProgramacionInversione', 'fkp_trimestre', 'pk_id_descripcion_parametrica');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ppSeguimientoFuncionamientos()
    {
        return $this->hasMany('App\PpSeguimientoFuncionamiento', 'fkp_trimestre', 'pk_id_descripcion_parametrica');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ppFormulacionFuncionamientos()
    {
        return $this->hasMany('App\PpFormulacionFuncionamiento', 'fkp_modalidad_contratacion', 'pk_id_descripcion_parametrica');
    }


}
