<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class G_garantia extends Model
{
    use HasFactory;

    protected $table = 'g_garantias';

    protected $fillable = [
        'pk_id_garantia',
        'codigo_garantia',
        'fkp_entidad_financiera',
        'fkp_estado',
        'importe',
        'fkp_moneda',
        'correlativo_tesoreria',
        'fkp_tipo_garantia',
        'fk_id_unidad_organizacional',
        'unidad_organizacional',
        'fk_id_proyecto',
        'nombre_proyecto',
        'cuce',
        'afianzado',
        'fk_id_afianzado',
        'tramo',
        'sisin',
        'fecha_vencimiento',
        'fecha_emision',
        'fecha_ingreso_abc',
        'fecha_liberacion',
        'verificacion_legal',
        'verificacion_tecnica',
        'nur_nuri',
        'fk_user',
        'activo',
        'directorio',
        'numero_contrato',
        'importe_ejecutado',
        'fk_contrato_siin',
        'fk_proyecto_ficha',
        'codigo_proceso',
        'garantia_requerimiento',
        'archivo',
        'fk_id_empresa',
        'fk_id_obra',
    ];
    protected $guarded = ['pk_id_garantia','created_at','updated_at'];
    protected $primaryKey = 'pk_id_garantia';

    public function G_empresa()
    {
        return $this->belongsTo('App\Models\G_empresa', 'fk_id_empresa', 'pk_id_empresa');
    }

    public function G_obra()
    {
        return $this->belongsTo('App\Models\G_obra', 'fk_id_obra', 'pk_id_obra');
    }

    public function G_liberar_garantia()
    {
        return $this->hasMany('App\Models\G_liberar_garantia', 'fk_id_liberar_garantia', 'pk_id_liberar_garantia');
    }

    public function G_enviar_correo()
    {
        return $this->hasMany('App\Models\G_enviar_correo', 'fk_id_garantia', 'pk_id_garantia');
    }

}
