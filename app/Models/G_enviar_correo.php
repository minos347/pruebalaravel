<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class G_enviar_correo extends Model
{
    use HasFactory;

    protected $fillable = [
        'correo_usuario',
        'nombre_usuario',
        'fk_user',
        'activo',
        'fk_id_garantia',
    ];

    protected $table = 'g_enviar_correos';
    protected $guarded = ['pk_id_enviar_correo', 'created_at', 'updated_at'];
    protected $primaryKey = 'pk_id_enviar_correo';

    public function G_garantia()
    {
        return $this->belongsTo('App\Models\G_garantia', 'fk_id_garantia', 'pk_id_garantia');
    }

}
