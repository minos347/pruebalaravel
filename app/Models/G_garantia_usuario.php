<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class G_garantia_usuario extends Model
{
    use HasFactory;

    protected $table = 'g_garantia_usuarios';

    protected $fillable = [
        'pk_id_garantia_usuario',
        'codigo_garantia',
        'cuce',
        'empresa',
        'afianzado',
        'fk_id_user',
        'fk_user',
        'activo',
        'fk_id_garantia',
    ];

    protected $guarded = ['pk_id_garantia_usuario','created_at','updated_at'];
    protected $primaryKey = 'pk_id_garantia_usuario';

    public function G_garantia()
    {
        return $this->hasMany('App\Models\G_garantia', 'fk_id_garantia', 'pk_id_garantia');
    }

}
