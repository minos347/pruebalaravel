<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class G_ejecutar_garantia extends Model
{
    use HasFactory;

    protected $fillable = [
        'importe_ejecutado',
        'fkp_moneda',
        'justificacion',
        'fkp_tipo_justificacion',
        'fecha_ejecucion',
        'nur_nuri',
        'archivo',
        'fk_user',
        'activo',
        'fk_id_garantia',
    ];

    protected $table = 'g_ejecutar_garantias';
    protected $guarded = ['pk_id_ejecutar_garantia','created_at','updated_at'];
    protected $primaryKey = 'pk_id_ejecutar_garantia';

    public function G_garantia()
    {
        return $this->hasMany('App\Models\G_garantia', 'fk_id_garantia', 'pk_id_garantia');
    }
}
