<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class G_renovacion_garantia extends Model
{
    use HasFactory;
    protected $fillable = [
        'pk_id_renovacion_garantia',
        'codigo_garantia',
        'fkp_tipo_garantia',
        'importe',
        'fkp_tipo_modena',
        'fecha_vencimiento',
        'fk_user',
        'activo',
        'fk_id_renovacion',
    ];

    protected $table = 'g_renovacion_garantias';
    protected $guarded = ['pk_id_renovacion_garantia','created_at','updated_at'];
    protected $primaryKey = 'pk_id_renovacion_garantia';

    public function G_renovacion()
    {
        return $this->hasMany('App\Models\G_renovacion', 'fk_id_renovacion', 'pk_id_renovacion');
    }
}
