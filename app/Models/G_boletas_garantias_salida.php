<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class G_boletas_garantias_salida extends Model
{
    use HasFactory;

    protected $fillable = [
        'fecha_ejecucion',
        'cuenta_bancaria',
        'numero_deposito',
        'fecha_boleta',
        'monto_boleta',
        'fkp_tipo_moneda',
        'archivo',
        'fk_user',
        'activo',
        'fk_id_boleta_garantia',
    ];

    protected $table = 'g_boletas_garantias_salidas';
    protected $guarded = ['pk_id_boleta_garantia_salida','created_at','updated_at'];
    protected $primaryKey = 'pk_id_boleta_garantia_salida';

    public function G_boletas_garantia()
    {
        return $this->belongsTo('App\Models\G_boletas_garantia', 'fk_id_boleta_garantia', 'pk_id_boleta_garantia');
    }



}
