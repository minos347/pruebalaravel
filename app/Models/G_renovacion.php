<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class G_renovacion extends Model
{
    use HasFactory;
    protected $fillable = [
        'pk_id_renovacion',
        'codigo_garantia',
        'fkp_tipo_garantia',
        'importe',
        'fkp_tipo_modena',
        'fecha_emision',
        'fecha_ingreso',
        'fecha_vencimiento',
        'justificacion',
        'nur_nuri',
        'codigo_garantia_renovado',
        'modelo_carta',
        'archivo',
        'fk_user',
        'activo',
        'fk_id_garantia',
    ];

    protected $table = 'g_renovacions';
    protected $guarded = ['pk_id_renovacion','created_at','updated_at'];
    protected $primaryKey = 'pk_id_renovacion';

    public function G_garantia()
    {
        return $this->hasMany('App\Models\G_garantia', 'fk_id_garantia', 'pk_id_garantia');
    }
}
