<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class G_obra extends Model
{
    use HasFactory;
    protected $fillable = [
        'pk_id_obra','obra','descripcion','fk_user','activo','fkp_departamento'
    ];

    protected $table = 'g_obras';
    protected $guarded = ['pk_id_obra','created_at','updated_at'];
    protected $primaryKey = 'pk_id_obra';

    public function G_garantia()
    {
        return $this->hasMany('App\Models\G_garantia', 'fk_id_obra', 'pk_id_obra');
    }
}
