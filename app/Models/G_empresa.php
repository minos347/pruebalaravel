<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class G_empresa extends Model
{
    use HasFactory;
    protected $fillable = [
        'pk_id_empresa','empresa','representante_legal','telefono','fax','celular','direccion','correo','fk_user','activo','fkp_departamento'
    ];

    protected $table = 'g_empresas';
    protected $guarded = ['pk_id_empresa','created_at','updated_at'];
    protected $primaryKey = 'pk_id_empresa';

    public function G_garantia()
    {
        return $this->hasMany('App\Models\G_garantia', 'fk_id_empresa', 'pk_id_empresa');
    }

}
