<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class G_boletas_garantia extends Model
{
    use HasFactory;

    protected $fillable = [
        'fecha_ejecucion',
        'cuenta_bancaria',
        'numero_deposito',
        'fecha_boleta',
        'monto_boleta',
        'fkp_tipo_moneda',
        'archivo',
        'fk_user',
        'activo',
        'fk_id_ejecutar_garantia',
    ];

    protected $table = 'g_boletas_garantias';
    protected $guarded = ['pk_id_boleta_garantia','created_at','updated_at'];
    protected $primaryKey = 'pk_id_boleta_garantia';

    public function G_ejecutar_garantia()
    {
        return $this->hasMany('App\Models\G_ejecutar_garantia', 'fk_id_ejecutar_garantia', 'pk_id_ejecutar_garantia');
    }

    public function G_boletas_garantias_salida()
    {
        return $this->hasMany('App\Models\G_boletas_garantias_salida', 'fk_id_boleta_garantia_salida', 'pk_id_boleta_garantia_salida');
    }
}
